Format: http://dep.debian.net/deps/dep5/
Upstream-Name: pyFltk
Upstream-Contact: Andreas Held <a.held@computer.org>
Source: http://pyfltk.sourceforge.net/

Files: contrib/*
Copyright: 2002, Greg Ercolano
License: LGPL-2

Files: contrib/Fl_Table.cxx
       contrib/Fl_Table.H
Copyright: 2002, Greg Ercolano
Copyright: 2004, O'ksi'D
License: LGPL-2

Files: fltk/docs/*
Copyright: 1998-2006, Bill Spitzak
           1998-2006, Michael Sweet
           1998-2006, Craig P. Earls
License: LGPL-2

Files: fltk/test/*
Copyright: 1998-2003, Bill Spitzak and others
           2003-2006, Andreas Held and others
License: LGPL-2

Files: fltk/test/fltk_threads.py
       fltk/test/sudoku.py
Copyright: 2005-2006, Michael Sweet.
           1998-1999, Bill Spitzak and others
           2003, Andreas Held and others
License: LGPL-2

Files: fltk/test/simple_table.py
       fltk/test/table.py
Copyright: 2003, Greg Ercolano
           2003, Andreas Held and others
           1998-1999, Bill Spitzak and others
License: LGPL-2

Files: fltk/test/TextEditor.py
Copyright: 2004, Chris Green
           2003, Andreas Held and others
           1998-1999, Bill Spitzak and others
License: LGPL-2

License: LGPL-2
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
  .
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.
  .
  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
  .
  On Debian systems, the complete text of the GNU Library General
  Public License can be found in `/usr/share/common-licenses/LGPL-2'.

Files: swig/*
Copyright: 1995-1998, The University of Utah and the Regents of the University
           of California
           1998-2005, University of Chicago
           2005-2006, Arizona Board of Regents (University of Arizona)
License: Expat

License: Expat
  I.
  Copyright (c) 1995-1998
  The University of Utah and the Regents of the University of California
  All Rights Reserved
  .  
  Permission is hereby granted, without written agreement and without
  license or royalty fees, to use, copy, modify, and distribute this
  software and its documentation for any purpose, provided that 
  (1) The above copyright notice and the following two paragraphs
  appear in all copies of the source code and (2) redistributions
  including binaries reproduces these notices in the supporting
  documentation.   Substantial modifications to this software may be
  copyrighted by their authors and need not follow the licensing terms
  described here, provided that the new terms are clearly indicated in
  all files where they apply.
  .
  IN NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF CALIFORNIA, THE 
  UNIVERSITY OF UTAH OR DISTRIBUTORS OF THIS SOFTWARE BE LIABLE TO ANY
  PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
  DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
  EVEN IF THE AUTHORS OR ANY OF THE ABOVE PARTIES HAVE BEEN ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
  .
  THE AUTHOR, THE UNIVERSITY OF CALIFORNIA, AND THE UNIVERSITY OF UTAH
  SPECIFICALLY DISCLAIM ANY WARRANTIES,INCLUDING, BUT NOT LIMITED TO, 
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND 
  THE AUTHORS AND DISTRIBUTORS HAVE NO OBLIGATION TO PROVIDE MAINTENANCE,
  SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
  .
  II.
  This software includes contributions that are Copyright (c) 1998-2005
  University of Chicago.
  All rights reserved.
  .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  .
  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.  Redistributions
  in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.  Neither the name of
  the University of Chicago nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY OF CHICAGO AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF
  CHICAGO OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  .
  III. 
  This software includes contributions that are Copyright (c) 2005-2006
  Arizona Board of Regents (University of Arizona).
  All Rights Reserved
  .
  Permission is hereby granted, without written agreement and without
  license or royalty fees, to use, copy, modify, and distribute this
  software and its documentation for any purpose, provided that 
  (1) The above copyright notice and the following two paragraphs
  appear in all copies of the source code and (2) redistributions
  including binaries reproduces these notices in the supporting
  documentation. Substantial modifications to this software may be
  copyrighted by their authors and need not follow the licensing terms
  described here, provided that the new terms are clearly indicated in
  all files where they apply.
  .
  THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY OF ARIZONA AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF
  ARIZONA OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2009, Luca Falavigna <dktrkranz@debian.org>
           2011, Charlie Smotherman <cjsmo@cableone.net>
           2019, Robert Arkiletian <robark@gmail.com>
License: GPL-3

License: GPL-3
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  .
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  .
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  .
  On Debian systems, the complete text of the GNU General
  Public License can be found in `/usr/share/common-licenses/GPL-3'.
