/* File : Fl_Tooltip.i */
//%module Fl_Tooltip

%feature("docstring") ::Fl_Tooltip
"""
The Fl_Tooltip class provides tooltip support for all FLTK widgets.
""" ;

%{
#include "FL/Fl_Tooltip.H"
%}

%include "FL/Fl_Tooltip.H"
