Format: 3.0 (quilt)
Source: pyfltk
Binary: python3-fltk, python3-fltk-doc, python3-fltk-dbg
Architecture: any all
Version: 1.3.4.1-1
Maintainer: Robert Arkiletian <robark@gmail.com>
Homepage: http://pyfltk.sourceforge.net/
Build-Depends: debhelper (>= 12), python3-all-dev (>= 3.7.2-1), python3-all-dbg (>= 3.7.2-1), libfltk1.3-dev, mesa-common-dev
Package-List:
 python3-fltk deb python optional arch=any
 python3-fltk-dbg deb debug extra arch=any
 python3-fltk-doc deb doc optional arch=all
Checksums-Sha1:
 6055bedf0682edb95d3bcbb7bc925a93b887293d 955717 pyfltk_1.3.4.1.orig.tar.gz
 1ba3cb8a6ac92831b1186e278ca0b15fb7a38b1a 5092 pyfltk_1.3.4.1-1.debian.tar.xz
Checksums-Sha256:
 9cbac496f124cd99a1eb0d329dbfdac0d1e160d25f9fd82a3dfb10abc86e069c 955717 pyfltk_1.3.4.1.orig.tar.gz
 c40eeefc557e7c649896096641b82599812e42e1c6613209ead11b37f6f20bc9 5092 pyfltk_1.3.4.1-1.debian.tar.xz
Files:
 eeadb341c820541480a5a21f9963ba3e 955717 pyfltk_1.3.4.1.orig.tar.gz
 134b727d123d97d2d6c3715404165aa5 5092 pyfltk_1.3.4.1-1.debian.tar.xz
