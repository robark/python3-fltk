/* File : Fl_Toggle_Button.i */
//%module Fl_Toggle_Button

%{
#include "FL/Fl_Toggle_Button.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Toggle_Button)

%include "FL/Fl_Toggle_Button.H"
