/* File : Fl_Tree_Prefs.i */

%feature("docstring") ::Fl_SliderTree_Prefs
"""
This file contains the definitions for Fl_Tree's preferences.
""" ;

%{
#include "FL/Fl_Tree_Prefs.H"
%}

//%include "macros.i"

//CHANGE_OWNERSHIP(Fl_Tree_Prefs)

%include "FL/Fl_Tree_Prefs.H"
