/* File : Fl_Multi_Label.i */
//%module Fl_Multi_Label

%{
#include "FL/Fl_Multi_Label.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Multi_Label)

%include "FL/Fl_Multi_Label.H"
